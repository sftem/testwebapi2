﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using TestWebApi2.Models;

namespace TestWebApi2.Controllers
{
    public class projectsController : ApiController
    {
        private TestWebApi2Entities db = new TestWebApi2Entities();

        // GET: api/projects
        public IQueryable<projects> Getprojects()
        {
            return db.projects;
        }

        // GET: api/projects/5
        [ResponseType(typeof(projects))]
        public IHttpActionResult Getprojects(int id)
        {
            projects projects = db.projects.Find(id);
            if (projects == null)
            {
                return NotFound();
            }

            return Ok(projects);
        }

        // PUT: api/projects/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putprojects(int id, projects projects)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != projects.id)
            {
                return BadRequest();
            }

            db.Entry(projects).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!projectsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/projects
        [ResponseType(typeof(projects))]
        public IHttpActionResult Postprojects(projects projects)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.projects.Add(projects);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = projects.id }, projects);
        }

        // DELETE: api/projects/5
        [ResponseType(typeof(projects))]
        public IHttpActionResult Deleteprojects(int id)
        {
            projects projects = db.projects.Find(id);
            if (projects == null)
            {
                return NotFound();
            }

            db.projects.Remove(projects);
            db.SaveChanges();

            return Ok(projects);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool projectsExists(int id)
        {
            return db.projects.Count(e => e.id == id) > 0;
        }
    }
}